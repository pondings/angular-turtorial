import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import {TransferModule} from './transfer/transfer.module';
import {MockDataService,BranchService,ProductListService,ColorService} from './shared/services';

import { BsDropdownModule  } from 'ngx-bootstrap';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BsDropdownModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule,
    CoreModule,
    SharedModule,
    TransferModule,
    AppRoutingModule,
    InMemoryWebApiModule.forRoot(MockDataService)
  ],
  providers: [BranchService,ProductListService,ColorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
