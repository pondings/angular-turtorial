import { Component, OnInit, ViewChild,Output,EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder,Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { Product,SerialNumber,Vehicle } from '../../shared/models';
import { ModalProductSearchComponent } from '../modal-product-search/modal-product-search.component';
import {ModalSerialComponent} from '../modal-serial/modal-serial.component';

@Component({
  selector: 'app-modal-product',
  templateUrl: './modal-product.component.html',
  styleUrls: ['./modal-product.component.scss']
})
export class ModalProductComponent implements OnInit {

  @ViewChild('prodModal') prodModal: ModalDirective;
  @ViewChild(ModalProductSearchComponent) prodSearchModal: ModalDirective;
  @ViewChild(ModalSerialComponent) serialModal : ModalSerialComponent;
  @Output() addVehicle : EventEmitter<Vehicle> = new EventEmitter();

  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.createForm();
  }


  ngOnInit() {
  }

  private onSubmit(form: any) {
    this.addVehicle.emit(form);
    this.form.reset();
    this.hide();
  }

  private createForm() {
    this.form = this.fb.group({
      product: [null, Validators.required],
      price: ['', Validators.required],
      snList: [null, Validators.required]
    });
  }

  private setSerialList(serialList : SerialNumber[]){
    this.form.patchValue({
      snList : serialList
    });
  }

  show() {
    this.prodModal.show();
  }

  hide() {
    this.prodModal.hide();
  }

  showModalProductSearch() {
    this.prodSearchModal.show()
  }
  
  private showSerialModal(){
    this.serialModal.show();
  }

  private setProd(prod: Product) {
    this.form.patchValue({
      product : prod
    });
  }


}
