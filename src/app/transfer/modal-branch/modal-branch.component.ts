import { Component, OnInit,ViewChild,Output,EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

import {BranchService} from '../../shared/services'
import {Branch} from '../../shared/models'


@Component({
  selector: 'app-modal-branch',
  templateUrl: './modal-branch.component.html',
  styleUrls: ['./modal-branch.component.scss']
})
export class ModalBranchComponent implements OnInit {

  @Output() selectedBranch : EventEmitter<Branch> = new EventEmitter();
  @ViewChild('branchModal') branchModal : ModalDirective;
  private branchs : BranchService[] = [];

  constructor(private branchService : BranchService) { }

  ngOnInit() {
    this.branchService.showAll().subscribe(rs => this.getBranchList(rs));
  }

  show(){
    this.branchModal.show();
  }

  hide(){
    this.branchModal.hide();
  }

  private getBranchList(branchs : any){
    this.branchs = branchs;
  }

  private selectBranch(branch : Branch){
    this.selectedBranch.emit(branch);
    this.hide();
  }

}
