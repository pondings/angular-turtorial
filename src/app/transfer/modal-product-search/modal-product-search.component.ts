import { Component, OnInit,ViewChild,Output,EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

import {ProductListService} from '../../shared/services';
import {Product,ProductGroup} from '../../shared/models';

@Component({
  selector: 'app-modal-product-search',
  templateUrl: './modal-product-search.component.html',
  styleUrls: ['./modal-product-search.component.scss']
})
export class ModalProductSearchComponent implements OnInit {

  @ViewChild('prodSearchModal') prodSearchModal : ModalDirective ;
  @Output() selectedProd : EventEmitter<Product> = new EventEmitter();
  private prodList : Product[];
  private selectedRow : String;

  constructor(private productListService : ProductListService) { }

  ngOnInit() {
    this.productListService.showAll().subscribe(rs => this.getProductList(rs));
  }

  show(){
    this.prodSearchModal.show();
  }

  hide(){
    this.prodSearchModal.hide();
  }

  private getProductList(prodList : Product[]){
    this.prodList = prodList;
  }

  private selectProd(item : Product){
    this.selectedRow = item.code;
  }

  private getProd(item:Product){
    this.selectedProd.emit(item);
    this.hide();
  }


}
