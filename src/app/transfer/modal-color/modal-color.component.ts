import { Component, OnInit,ViewChild,Output,EventEmitter } from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap/modal';
import {Color} from '../../shared/models';
import {ColorService} from '../../shared/services';


@Component({
  selector: 'app-modal-color',
  templateUrl: './modal-color.component.html',
  styleUrls: ['./modal-color.component.scss']
})
export class ModalColorComponent implements OnInit {

  @ViewChild('colorModal') colorModal : ModalDirective ;
  @Output() selectedColor : EventEmitter<Color> = new EventEmitter();
  private selectedRow : String;
  private colorList : Color[] = [];


  constructor(private colorService : ColorService) { }

  ngOnInit() {
    this.colorService.showAll().subscribe(rs => this.getColorList(rs));
  }

  private selectColor(item : Color){
    this.selectedRow = item.code;
  }

  private getColor(item : Color){
    this.selectedColor.emit(item);
    this.colorModal.hide();
  }

  private getColorList(colorList : Color[]){
    this.colorList = colorList;
  }

  show(){
    this.colorModal.show();
  }

  hide(){
    this.colorModal.hide()
  }



}
