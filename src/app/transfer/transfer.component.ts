import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ModalProductComponent } from './modal-product/modal-product.component';
import { ModalBranchComponent } from './modal-branch/modal-branch.component';
import { Branch, Transfer,Vehicle } from '../shared/models';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss']
})
export class TransferComponent implements OnInit {

  @ViewChild(ModalProductComponent) prodModal: ModalProductComponent;
  @ViewChild(ModalBranchComponent) branchModal: ModalBranchComponent;
  private vehicleList : Vehicle[] = []; 
  private form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.createForm();
  }

  private createForm() {
    this.form = this.fb.group({
      toBranch: this.fb.group({
        code: ['', Validators.required],
        name: [''],
        address: [''],
        tel: [''],
      })
    });
  }

  ngOnInit() {
  }

  private addVehicle(vehicle : Vehicle){
    this.vehicleList.push(vehicle);
  }

  private onSubmit(form: any) {
    console.log(form);
  }

  private openProdModal() {
    this.prodModal.show();
  }

  private openBranchModal() {
    this.branchModal.show();
  }

  private setBranch(branch: Branch) {
    this.form.patchValue({
      toBranch : branch
    });
  }
}
