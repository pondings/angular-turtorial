import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { SerialNumber,Color,Product } from '../../shared/models';
import { ModalColorComponent } from '../modal-color/modal-color.component';

@Component({
  selector: 'app-modal-serial',
  templateUrl: './modal-serial.component.html',
  styleUrls: ['./modal-serial.component.scss']
})
export class ModalSerialComponent implements OnInit {

  @ViewChild('serialModal') serialModal: ModalDirective;
  @ViewChild(ModalColorComponent) colorModal: ModalColorComponent;
  @Output() addSerialList : EventEmitter<SerialNumber[]> = new EventEmitter();

  private serialList: SerialNumber[] = [];
  private form: FormGroup;
  private editIndex : number;

  constructor(private fb: FormBuilder) {
    this.createForm();
  }

  ngOnInit() { }

  private createForm() {
    this.form = this.fb.group({
      id: '',
      engineNumber: '',
      bodyNumber: '',
      color: null,
      regNumber: '',
      keyNumber: '',
      product: null
    });
  }

  private prepareEdit(item : SerialNumber , index : number){
    this.editIndex = index;
    this.form.patchValue(item);
  }

  private editItem(form : any){
    this.serialList[this.editIndex] = form;
    this.editIndex = null;
  }

  private createItem(form : any){
    this.serialList.push(form);
  }

  private removeItem(index : number){
    this.serialList.splice(index,1);
  }

  private removeAll(){
    this.serialList = [];
  }

  private onSubmit(form : any){
    if(this.editIndex != null){
      this.editItem(form);
    }else{
      this.createItem(form);
    }
    this.form.reset();
  }

  private setColor(color : Color){
    this.form.patchValue({
      color : color
    });
  }

  private add(){
    this.addSerialList.emit(this.serialList);
    this.form.reset();
    this.serialList = [];
    this.hide();
  }

  private showColorModal(){
    this.colorModal.show();
  }

  show() {
    this.serialModal.show();
  }

  hide() {
    this.serialModal.hide();
  }

}
