import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { TransferRoutingModule } from './transfer-routing.module';
import { TransferComponent } from './transfer.component';
import { ModalProductComponent } from './modal-product/modal-product.component';

import { ModalModule  } from 'ngx-bootstrap';
import { ModalBranchComponent } from './modal-branch/modal-branch.component';
import { ModalProductSearchComponent } from './modal-product-search/modal-product-search.component';
import { ModalSerialComponent } from './modal-serial/modal-serial.component';
import { ModalColorComponent } from './modal-color/modal-color.component';

@NgModule({
  imports: [
    CommonModule,
    TransferRoutingModule,
    ReactiveFormsModule,
    ModalModule.forRoot()
  ],
  declarations: [TransferComponent, ModalProductComponent, ModalBranchComponent, ModalProductSearchComponent, ModalSerialComponent, ModalColorComponent]
})
export class TransferModule { }
