import {Product,Color} from './';

export class SerialNumber {
    id: number;
    engineNumber: string;
    bodyNumber: string;
    color: Color;
    regNumber: string;
    keyNumber: string;
    product: Product;
}
