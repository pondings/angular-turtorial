import {Product,SerialNumber} from './';

export class Vehicle {
    id: number;
    product: Product;
    price: number;
    snList: SerialNumber[];
}
