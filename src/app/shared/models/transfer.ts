import {Branch,Vehicle} from './';

export class Transfer {
    id: number;
    code: string;
    fromBranch: Branch;
    toBranch: Branch;
    vehicle: Vehicle[];
}
