import {ProductGroup} from './';

export class Product {
    id: number;
    code: string;
    name: string;
    unit: string;
    group: ProductGroup;
}
