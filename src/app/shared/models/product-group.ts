export class ProductGroup {
    id: number;
    code: string;
    name: string;
}
