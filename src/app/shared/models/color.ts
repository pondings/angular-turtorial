export class Color {
    id: number;
    code: string;
    name: string;
}
