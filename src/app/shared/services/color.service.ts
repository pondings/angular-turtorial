import { Injectable } from '@angular/core';
import {Http} from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class ColorService {

  private url ='api/colors';

  constructor(private http : Http) { }

  showAll(){
    return this.http.get(this.url).map(rs => rs.json().data);
  }
}
